#-*- encoding utf-8 -*-

from lireFichier import *
import numpy as np
import sys
import re
import math
import operator
import cx_Oracle
import random
from cluster import *
from fonctionsEntrainement import *
from fonctionsRecherche import *

def connexionBD():#se connecter à la bd
	PATH_ORACLE = 'C:\Oracle\Client\product\12.2.0\client_1\bin'
	sys.path.append(PATH_ORACLE)
	
	dsn_tns = cx_Oracle.makedsn('delta', 1521, 'decinfo')
	chaineCnx = 'e1526952' + '/' + 'BBBbbb111' + '@' + dsn_tns
	
	cnx = cx_Oracle.connect(chaineCnx)
	print("connexion etablie")
	
	return cnx

#################################################

def argumentsLigneDeCommande():
	mode = "cluster"
	fenetre = 5
	chemin = None
	encodage = None
	nc = random.randint(2, 4)
	listeDeMotsCentroides = None
	nbMotsAAfficher = 10
	try:
		if "-e" in sys.argv:
			mode = "remplirBD"
		elif "-s" in sys.argv:
			mode = "lectureBD"
		else:
			mode = "cluster"
	except:
		print("Erreur lors de la lecture du mode d'ouverture du programme")

	try:
		if "-t" in sys.argv:
			index = sys.argv.index("-t")
			fenetre = int(sys.argv[index + 1])
		else:
			raise Exception("Pas de taille de fenetre!")
		#fenetre = int(sys.argv[1])
	except:
		print("Pas de taille de fenêtre passée en paramètre, \'5\' pris par défaut.\n")

	if mode == "remplirBD":##TP2!!!!!!!!!!!!!!!!!!!!!!

		if "-enc" in sys.argv:
			index = sys.argv.index("-enc")
			encodage = sys.argv[index + 1]
		else:
			print("Pas d\'encodage passé en paramètre, \'UTF-8\' pris par défaut.\n")
			encodage = "utf-8"
			
		if "-cc" in sys.argv:
			index = sys.argv.index("-cc")
			chemin = sys.argv[index + 1]
		else:
			chemin = "texte.txt"
			print("Pas de fichier passé en paramètre, \'texte.txt\' pris par défaut.")

	else:##TP3!!!!!!!!!!!!!!!!!!!!!!!!!!!
		try:
			if("-nc" in sys.argv):
				index = sys.argv.index("-nc")
				nc = sys.argv[index + 1]
				nc = int(nc)
		except:
			print("Erreur lors de la lecture du nombre de 'clusters'")

		try:
			if("-m" in sys.argv):
				index = sys.argv.index("-m")
				MotsCentroides = sys.argv[index + 1]
				listeDeMotsCentroides = MotsCentroides.split(" ")
				nbClusters = len(listeDeMotsCentroides)
		except:
			print("Erreur lors de la lecture de la liste de mots centroides")

		try:
			if("-n" in sys.argv):
				index = sys.argv.index("-n")
				nbMotsAAfficher = sys.argv[index + 1]
				nbMotsAAfficher = int(nbMotsAAfficher)
		except:
			print("Erreur lors de la lecture du nombre de mots à afficher")

	return mode, fenetre, encodage, chemin, nc, listeDeMotsCentroides, nbMotsAAfficher
	
def main():
	#on prend les arguments de la ligne de commande
	mode, fenetre, encodage, chemin, nbClusters, listeDeMotsCentroides, nbMotsAAfficher = argumentsLigneDeCommande()

	print(mode)
	print(fenetre)
	print(encodage)
	print(chemin)
	print(nbClusters)
	print(listeDeMotsCentroides)

	if(mode == "remplirBD"):
		s = lireFichier(chemin, encodage)
		######################################ENTRAINEMENT######################################
		#sépare le texte en phrases et en mots et place tout dans un tableau à deux dimensions
		tableau = separerTexte(s)
		del s
		dMots = remplirDictionnaire(tableau)
		remplirBD(dMots)
		remplirBDCoocurences(tableau, dMots, fenetre)
		exit()
	elif mode == "lectureBD":
        ###################RECHERCHE##################
		cnx = connexionBD()
		del encodage, chemin
		dMots,coocurence = rechercheBD(cnx,fenetre)
		matrice = remplirMatriceRecherche(dMots,coocurence)
		del fenetre
		dArret = remplirListeArret()
		return(rechercheUtilisateur(matrice, dMots, dArret))
	else:
		###################TP3 ICI!!##################
		print("calcul cluster")
		cnx = connexionBD()
		del encodage, chemin
		dMots,coocurence = rechercheBD(cnx,fenetre)
		matrice = remplirMatriceRecherche(dMots,coocurence)
		if listeDeMotsCentroides is not None:
			centroides, f = initCentroideListeMots(dMots,matrice,fenetre,nbMotsAAfficher, listeDeMotsCentroides)
		else:
			listeDeListeDeMots, f = initCentroideNbAleatoire(dMots,matrice,fenetre,nbMotsAAfficher, nbClusters)
			centroides = calculerCentroides(listeDeListeDeMots, matrice)
		
		boucleDEvenement(centroides,matrice, f, dMots, nbMotsAAfficher)
		
		pass

if __name__ == '__main__':
    main()
