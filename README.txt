Duchesne, Florent
Prud'Homme,Simon
Larbi, Berkane-Hicheme

Le script SQL est fonctionnel, le script python est fonctionnel, les scripts batch sont fonctionnels, la recette de gâteau est fonctionnelle.

Informations de connexion: Utilisateur: e1526952 - mot de passe: BBBbbb111

RECETTE GATEAU : 
2 tasses	de farine
2 c. à thé	de poudre à pâte Magic
1 c. à thé	de bicarbonate de soude
1 1/2 c. à thé	de cannelle
1/2 c. à thé	de gingembre moulu et 1/2 c. à thé de muscade moulue
1/2 tasse	de ketchup aux tomates Heinz et 1/2 tasse d'eau
2 c. à soupe	de colorant alimentaire rouge
1 1/2 tasse	de beurre, ramolli, divisée
1 1/2 tasse	de cassonade foncée tassée
2	œufs
175 g	(environ 3/4 d'un paquet de 250 g) de fromage à la crème Philadelphia en brique, ramolli
1 c. à thé	de vanille
4 tasses	de sucre à glacer

Chauffer le four à 350 °F.
Vaporiser deux moules ronds (9 po) d'un enduit à cuisson; couvrir de papier sulfurisé le fond de chaque moule.
Mélanger la farine, la poudre à pâte, le bicarbonate de soude et les épices. Mélanger le ketchup, l'eau et le colorant alimentaire dans un bol à part jusqu'à homogénéité.
Mélanger au batteur 3/4 t du beurre et la cassonade dans un grand bol jusqu'à ce que la préparation soit légère et mousseuse. Incorporer les œufs, un à la fois. Ajouter la préparation de farine en alternant avec le mélange de ketchup, en mélangeant bien après chaque ajout. Verser la pâte dans les moules préparés.
Cuire 30 min ou jusqu'à ce qu'un cure-dents inséré au centre en ressorte propre. Laisser refroidir 10 min. Démouler et mettre sur des grilles; laisser refroidir complètement.
Mélanger au batteur le fromage à la crème, la vanille et le reste du beurre dans un autre grand bol jusqu'à consistance crémeuse. Ajouter graduellement le sucre à glacer jusqu'à ce que le mélange soit léger et mousseux.
Empiler les étages du gâteau dans une assiette; étaler le glaçage au fromage à la crème entre les étages, sur le dessus et les côtés.