#-*- encoding utf-8 -*-

def lireFichier(chemin, encodage = "utf-8"):
	try:
		f = open(chemin, encoding = encodage)
		s = f.read()
		f.close()
	except:
		print("Impossible d\'ouvrir un fichier.")
		return ""
	return s
