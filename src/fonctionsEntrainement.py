from main import *

def separerTexte(texteBrut):
	#met tout en minuscules
	texteBrut = str.lower(texteBrut)
	
	###expression régulière qui sépare les phrases en différentes string
	patron = '[.!?\n]+'
	p = re.compile(patron)
	l = p.split(texteBrut)

	tempo = []
	tableau = []

	#boucle qui, une phrase à la fois, met les mots dans un tableau
	patronMots = '[ ,:;.@*~/"\«\»\'\-\[\]_]()+'
	pMots = re.compile(patronMots)
	for i in range(len(l)):
		tempo = l[i]
		tableau.append(pMots.split(tempo))
	tableau.pop()#pour retirer la string vide à la fin du texte

	return tableau

def remplirDictionnaire(tableau):
	##crée une liste de mots uniques
	index = 0
	dMots = {}
	for i in range(len(tableau)):
		for j in range(len(tableau[i])):
			mot = tableau[i][j]
			if mot not in dMots:
				dMots[mot] = index
				index += 1
	return dMots

def remplirBD(dMots):
	cnx = connexionBD()
	cur = cnx.cursor()

	listeInsererDansBD = []
	nouveauMot = {}
	
	#id a partir du dernier id inscrit
	enonceIdCourrant = 'SELECT MAX(id) FROM mots'
	cur.execute(enonceIdCourrant)
	id = cur.fetchall()
	id = id[0][0]
	if id == None:
		id = -1

	#Enonces
	enonceMotExistant = 'SELECT * FROM mots'
	enonceInsert = 'INSERT INTO mots VALUES(:id, :mot)'
	#Chercher les mots existants dans la BD
	cur.execute(enonceMotExistant)
	motExistant = cur.fetchall()
	#Mettre les mots dans un dictionnaire
	dictMotExistant = {}
	for mot in motExistant:
		dictMotExistant[mot[1]] = mot[0]
		#print(mot[1])

	#pour tous les mots du dictionnaire
	for m in dMots:
		#Si c'est un mot valide qui n'est pas dans la bd
		if m != None and len(m) > 0 and m not in dictMotExistant:
			id += 1
			#Faire un dictionnaire contenant id + mot
			nouveauMot = {'id' : id, 'mot' : m}
			#ajouter le dictionnaire a la liste
			listeInsererDansBD.append(nouveauMot)
			

	cur.executemany(enonceInsert, listeInsererDansBD)
	cnx.commit()

def remplirBDCoocurences(tableau, dMots, fenetre):
	dictionnaireIMPORTANT = {}
	
	cnx = connexionBD()
	cur = cnx.cursor()

	dicoParametresInsert = {}
	dicoParametresUpdate = {}
	listeParametresInsert = []
	listeParametresUpdate = []

	enonceLireCooc = 'SELECT id_mot1, id_mot2, nb_cooc, taille_fenetre FROM coocurence WHERE taille_fenetre = :fenetre'
	dicoFenetre = {"fenetre" : fenetre}
	cur.execute(enonceLireCooc, dicoFenetre)

	coocDeBD = cur.fetchall()
	tupleMagique = ()

	for entree in coocDeBD:
		tupleMagique = (entree[0], entree[1], entree[3])
		dictionnaireIMPORTANT[tupleMagique] = entree[2]
		dicoParametresUpdate[tupleMagique] = entree

	enonceIDCorrespondant = 'SELECT id, chaine FROM mots'

	cur.execute(enonceIDCorrespondant)

	listeMotsBD = cur.fetchall()

	dico = {}

	for mot in listeMotsBD:
		dico[mot[1]] = mot[0]

	for phrase in tableau:
		debut =  -int(fenetre / 2)
		if(len(phrase) > 1):
			fin = fenetre + debut
		else:
			fin = 0
		for mot in phrase:
			if(fin > len(phrase)):
				fin = len(phrase)
			for motDansFenetre in range(debut, fin):
				if(phrase.index(mot) != motDansFenetre):
					if(motDansFenetre >= 0):
						#mot
						if(mot != '' and phrase[motDansFenetre] != ''):
							if(dictionnaireIMPORTANT.get((dico[mot], dico[phrase[motDansFenetre]], fenetre), None) is None):#on vérifie si un mot n'est pas déjà dans la BD ou dans nos dictionnaires
								dictionnaireIMPORTANT[(dico[mot], dico[phrase[motDansFenetre]], fenetre)] = 1
								dicoParametresInsert = {"mot1" : dico[mot], "mot2" : dico[phrase[motDansFenetre]], "nbCooc" : dictionnaireIMPORTANT[(dico[mot], dico[phrase[motDansFenetre]], fenetre)], "fenetre" : fenetre}
								listeParametresInsert.append(dicoParametresInsert)
							else: #s'il est déjà dedans, on update son nombre de coocurences
								tupleMot = (dico[mot], dico[phrase[motDansFenetre]], dictionnaireIMPORTANT[(dico[mot], dico[phrase[motDansFenetre]], fenetre)], fenetre)
								dictionnaireIMPORTANT[(dico[mot], dico[phrase[motDansFenetre]], fenetre)] += 1
								if(tupleMot in dicoParametresInsert):#si le mot est dans le nouveau fichier texte mais pas dans la bd
									dicoParametresInsert= {"mot1" : dico[mot], "mot2" : dico[phrase[motDansFenetre]], "nbCooc" : dictionnaireIMPORTANT[(dico[mot], dico[phrase[motDansFenetre]], fenetre)] + 1, "fenetre" : fenetre}
									listeParametresInsert.append(dicoParametresInsert)
								else:#s'il est dans la bd
									dicoParametresUpdate = {"nbCooc" : tupleMot[2] + 1, "id1" : tupleMot[0], "id2" : tupleMot[1], "fenetre" : tupleMot[3]}
									listeParametresUpdate.append(dicoParametresUpdate)

			debut += 1
			fin += 1
	enoncePushCooc = 'INSERT INTO coocurence VALUES(:mot1, :mot2, :nbCooc, :fenetre)'
	cur.executemany(enoncePushCooc, listeParametresInsert)

	enoncePushCooc = 'UPDATE coocurence SET nb_cooc = :nbCooc WHERE id_mot1 = :id1 AND id_mot2 = :id2 AND taille_fenetre = :fenetre'
	cur.executemany(enoncePushCooc, listeParametresUpdate)

	cnx.commit()
	print("commit!")
	return