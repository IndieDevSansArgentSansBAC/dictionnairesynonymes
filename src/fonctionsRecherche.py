from main import *

def rechercheBD(cnx,fenetre):#va rechercher du vocabulaire de même que toutes les coocurences de chaque mot de la fenetre donnée dans la bd 
	print("recherche en cours ...")
	cur = cnx.cursor()
	
	dMots={}
	coocurence = []
	
	enonce='SELECT id,chaine FROM mots'#va chercher les mots et leurs id
	cur.execute(enonce)
	
	for c in cur.fetchall():
		idMot=c[0]
		mot=c[1]
		dMots[mot]=idMot

	enonce='SELECT id_mot1,id_mot2,nb_cooc FROM coocurence WHERE taille_fenetre = :taille_fenetre'#va chercher les coocurences
	tf = {'taille_fenetre' : fenetre}
	cur.execute(enonce,tf)
	fetch = cur.fetchall()
	for c in fetch:
		idMot1=c[0]
		idMot2=c[1]
		nbCooc=c[2]
		coocurence.append([idMot1,idMot2,nbCooc])

	cur.close()
	cnx.close()
	
	return dMots,coocurence

def remplirMatriceRecherche(dMots,coocurence):#remplit la machine
	ln = len(dMots)+1

	matrice = np.zeros((ln,ln))

	for i in range(len(coocurence)):
		matrice[coocurence[i][0]][coocurence[i][1]] = coocurence[i][2]

	return matrice

def remplirListeArret():
	s = lireFichier("fichierListeArret.txt", "utf-8")
	s = str.lower(s)
	patron = '[ ]+'
	p = re.compile(patron)
	listeMotsArret = p.split(s)
	listeMotsArret.append("")
	return listeMotsArret

def rechercheUtilisateur(matrice, dMots, dArret):
	mot, nbSynonymes, methodeCalcul = "", 5, 0
	while(True):
		try:
			motrecherche = input("\
			\nEntrez un mot, le nombre de synonymes que vous voulez et la méthode de calcul :\
			\nProduit Scalaire : 0, Écart-type : 1, City-Block : 2\
			\n-1 pour quitter\n")
			
			if(motrecherche == "-1"):
				print("fermeture du programme")
				return(0)
			else:
				try:
					mot, nbSynonymes, methodeCalcul = motrecherche.split(" ")
					nbSynonymes = int(nbSynonymes)
				except:
					print("Recherche invalide, recommencez")
					continue
				if(mot not in dMots):
					print("le mot entré n\'est pas dans notre dictionnaire")
				else:
					if(methodeCalcul == "0"):#produit scalaire
						vect = produitScalaire(mot, matrice, dMots, nbSynonymes, dArret)
						for i in range(nbSynonymes):
							print(vect[i])
					elif(methodeCalcul == "1"):#écart à la moyenne
						vect = leastSquareOUcityBlock(mot, matrice, dMots, nbSynonymes, dArret, np.square)
						for i in range(nbSynonymes):
							print(vect[i])
					elif(methodeCalcul == "2"):#cityblock
						vect = leastSquareOUcityBlock(mot, matrice, dMots, nbSynonymes, dArret, abs)
						for i in range(nbSynonymes):
							print(vect[i])
					else:
						print("La méthode de calcul doit se trouver entre 0 et 2 inclusivement!")
		except KeyboardInterrupt:
			print("\nfermeture du programme demandée par l\'utilisateur")
			return 0

def puissance2(valeur):
	return valeur**2

def produitScalaire(motrecherche, matrice, listeMots, nbSynonymes, dArret):
	index = listeMots[motrecherche]
	vRecherche = matrice[index]
	dico = {}
	for i in listeMots:
		if(i not in dArret):
			if(i != motrecherche):
				dico[i] = np.dot(vRecherche, matrice[listeMots[i]])
	vecteur = sorted(dico.items(), key=operator.itemgetter(1), reverse = True)
	return vecteur[0:nbSynonymes]

def leastSquareOUcityBlock(motrecherche, matrice, listeMots, nbSynonymes, dArret, fonction):
	index = listeMots[motrecherche]
	vRecherche = matrice[index]
	sommeVecteurs = {}
	for i in listeMots:
		if(i not in dArret):
			if(i != motrecherche):
				sommeVecteurs[i] = (np.sum(fonction(matrice[listeMots[i]] - vRecherche)))
	vecteur = sorted(sommeVecteurs.items(), key=operator.itemgetter(1), reverse = False)
	return vecteur[0:nbSynonymes]