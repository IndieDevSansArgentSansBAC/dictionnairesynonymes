DROP TABLE coocurence;
DROP TABLE mots;

CREATE TABLE mots (
	id				NUMBER(10),
	chaine				VARCHAR2(26) NOT NULL,
	CONSTRAINT pk_mot PRIMARY KEY (id)
);

CREATE TABLE coocurence (
	id_mot1				  NUMBER(10),
  id_mot2         NUMBER(10),
  nb_cooc         NUMBER(38) NOT NULL,
  taille_fenetre  NUMBER(2),
	CONSTRAINT fk_mot1 FOREIGN KEY (id_mot1) REFERENCES mots(id),
  CONSTRAINT fk_mot2 FOREIGN KEY (id_mot2) REFERENCES mots(id),
  CONSTRAINT pk_cooc PRIMARY KEY (id_mot1, id_mot2, taille_fenetre)
);

SELECT * FROM mots;
SELECT MAX(id) FROM mots;
SELECT * FROM mots WHERE chaine = 'aux';