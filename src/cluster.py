#-*- encoding utf-8 -*-

import random
import numpy as np
import operator
import time
import sys, os
import time

def initCentroideNbAleatoire(dMots, matrice, fenetre, nbMotsAAfficher, nbClusters): #REVOIR# 
	
	date = time.strftime("%Y-%m-%d_%H-%M-%S")
	file_name = date +".txt"
	f = open(file_name, 'w')
	
	chemin = repr(os.getcwd())
	chemin = chemin.replace("'", "").replace("\\\\","\\")
	nom = os.path.basename(sys.argv[0])
	
	print("\n")
	ln=len(dMots)
	listeDeListesDeMots=[]
	for i in range(nbClusters):
		listeDeListesDeMots.append([])
	estEgal = False
	for i in range(len(matrice)):
		position = random.randrange(nbClusters)
		listeDeListesDeMots[position].append(matrice[i])
		#print(str(position) + " a " + str(len(listeDeListesDeMots[position])) + " nombre de mots")
	
	f.write(chemin + ">python " + nom + " -nc " + str(nbClusters) + " -n " + str(nbMotsAAfficher) + " -t " + str(fenetre) + "\n\n")
	
	f.write(str(nbClusters) + " centroïdes, générés aléatoirement.\n\n")
	return listeDeListesDeMots, f
	
	
def initCentroideListeMots(dMots, matrice,fenetre,nbMotsAAfficher,listeDeMotsCentroides = None): #REVOIR# 
	
	date = time.strftime("%Y-%m-%d_%H-%M-%S")
	file_name = date +".txt"
	f = open(file_name, 'w')
	
	nbClusters = len(listeDeMotsCentroides)
	
	chemin = repr(os.getcwd())
	chemin = chemin.replace("'", "").replace("\\\\","\\")
	nom = os.path.basename(sys.argv[0])
	
	centroides = []
	
	print("\n")
	ln=len(dMots)
	estEgal = False

	for motChoisi in listeDeMotsCentroides:
		centroides.append(matrice[dMots[motChoisi]])

	f.write(chemin + ">python " + nom + " -m " + str(listeDeMotsCentroides) + " -n " + str(nbMotsAAfficher) + " -t " + str(fenetre) + "\n\n")
	
	f.write(str(nbClusters) + " centroïdes, générés aléatoirement.\n\n")
	return centroides, f
	
def calculerCentroides(listeDeListeDeMots, matrice):
	centroides = []
	for listeDeMots in listeDeListeDeMots:
		centroides.append(np.mean(listeDeMots))
		
	return centroides

def leastsquare(v1, v2):
	return np.sum(np.square(v1 - v2))

def caclDist(centroides,matrice):
	listeDistCentMotCourant=[]
	
	for indexMotCourant in range(len(matrice)):
		vecteurs = []
		for vecteurCentroide in centroides:
			vecteurMotCourant = matrice[indexMotCourant]
			vecteurs.append(leastsquare(vecteurCentroide, vecteurMotCourant))#calcule la distance entre un point et le centroide
		listeDistCentMotCourant.append((vecteurMotCourant, vecteurs, indexMotCourant))#enregistre les distances entre tous les points et tous les centroides
	return listeDistCentMotCourant
	
def attribuerPoints(listeDistCentMotCourant,centroides):
	dict = {}
	listeCentroideEtMots = []
	listeTuples = []
	liste = []
	listeOrdonneeMotsParCentroide = []
	
	for noMot in listeDistCentMotCourant:
		indexDistancePlusPetite = np.argmin(noMot[1])#centroide le plus proche du mot
			
		tuple = (noMot[0], indexDistancePlusPetite, noMot[2])#
		listeTuples.append(tuple)#vecteur mot, distance pour un centroide
	
	for indexCentroide in range(len(centroides)):
		listeDeMotsPourUnCentroide = []
		listeMotsPlusProchesOrdonnes = []
		listeIndexMotCourant = []
		for tuple in listeTuples:
			if tuple[1] == indexCentroide:
				listeDeMotsPourUnCentroide.append(tuple[0])
				listeIndexMotCourant.append(tuple[2])
				########
				nouveauTuple = (tuple[2], listeDistCentMotCourant[tuple[2]][1][indexCentroide])#(index du mot dans la matrice, distance avec le centroide le plus proche)
				listeMotsPlusProchesOrdonnes.append(nouveauTuple)#contient des tuples (index du mot dans la matrice, index du centroide)

		listeCentroideEtMots.append((indexCentroide, listeDeMotsPourUnCentroide))
		liste.append(listeIndexMotCourant)
		
		listeMotsPlusProchesOrdonnes = sorted(listeMotsPlusProchesOrdonnes, key=operator.itemgetter(1))
		listeOrdonneeMotsParCentroide.append(listeMotsPlusProchesOrdonnes)
		
	return listeCentroideEtMots, liste, listeOrdonneeMotsParCentroide
	
def moyenneCentroides(listeCentroideEtMots, centroides):
	nouveauxCentroides = []
	for tuple in listeCentroideEtMots:
		if len(tuple[1]):
			nouveauCentroide = np.mean(tuple[1])
			nouveauxCentroides.append(nouveauCentroide)
		else:
			nouveauxCentroides.append(centroides[tuple[0]])
		
	return nouveauxCentroides
	
def boucleDEvenement(centroides,matrice,f, dMots, nbResultatsVoulu):
	
	premiersCentroides = []
	nouveauxCentroides = centroides
	iteration = 0
	listeCentroideEtMots = []
	listeIndexMotCourant = []
	ancienneListeIndexMotCourant = []
	start = time.time()
	
	while True:
		print("nouvelle itération")
		changements = 0
		ancienneListeIndexMotCourant = listeIndexMotCourant
		listeDistCentMotCourant = caclDist(nouveauxCentroides, matrice)
		
		listeCentroideEtMots, listeIndexMotCourant, listeOrdonneeMotsParCentroide = attribuerPoints(listeDistCentMotCourant,nouveauxCentroides)
		
		nouveauxCentroides =  moyenneCentroides(listeCentroideEtMots, nouveauxCentroides)
		
		premiersCentroides = nouveauxCentroides
		
		#itemsPartage = np.array_equal(premiersCentroides, nouveauxCentroides)
		
		if ancienneListeIndexMotCourant != []:
			for noCentroide in range(len(listeCentroideEtMots)):
				changements += len(set(ancienneListeIndexMotCourant[noCentroide]) - set(listeIndexMotCourant[noCentroide]))
		else:
			for noCentroide in range(len(nouveauxCentroides)): 
				f.write("Il y a " + str(len(listeIndexMotCourant[noCentroide])) + " points (mots) regroupes autour du centroïde no " + str(noCentroide) + ".\n")
			changements = len(matrice)
		
		f.write("\n============================================\n")
		f.write("Iteration " + str(iteration) + " terminee. " + str(changements) + " changements de clusters.\n\n")
		
		if changements != 0:
			for noCentroide in range(len(nouveauxCentroides)):
				f.write("Il y a " + str(len(listeCentroideEtMots[noCentroide][1])) + " points (mots) regroupes autour du centroïde no " + str(noCentroide) + ".\n")
		iteration += 1
		
		if changements == 0:
			
			for indexCentroide in range(len(nouveauxCentroides)):
				compteur = 0
				f.write(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>\n")
				f.write("Groupe " + str(indexCentroide) + " :\n\n")
				for i in range(nbResultatsVoulu):
					if i < len(listeDistCentMotCourant):
						if len(listeDistCentMotCourant[indexCentroide]) > i:
							f.write(str(list(dMots.keys())[list(dMots.values()).index(listeOrdonneeMotsParCentroide[indexCentroide][i][0])]))
							f.write(" --> ")
							f.write(str(listeOrdonneeMotsParCentroide[indexCentroide][i][1]) + " :\n\n")
						else:
							break
					else:
						break
			end = time.time()
			secondes = end - start
			f.write("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n")
			f.write("Clustering en " + str(iteration-1) + " itérations: " + str(secondes) + " secondes.\n")
			f.close()
			break
	